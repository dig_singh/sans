<?php

class Client_model extends CI_Model {

	public function __construct() {
		ini_set("display_errors", "on");
		error_reporting(E_ALL);
		$this->load->database();

	}

	
	function login($arr) {
		
		$result = array();
		 
        $email = $arr['email'];
        $password = $arr['password'];
       
	    if (preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/', $email))
		{
				$this->db->select("id,name");
                $this->db->from("users");
                $this->db->where_in('user_id', $arr);
                $qry = $this->db->get();
                $results = $qry->result_array();
               
	    } else {
               
        }

		 return $results;
	}
	
	function updateMovieList($arr) {
		$data = array('name' => $arr['group_name']);
		$this->db->where('id', $arr['id']);
		$res = $this->db->update('movie_list', $data);
		
		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message updateMovieList sucesss " ; 
             $details['status'] = "true";
			 $details['movie_list_id'] = $id;
			
		}
		return  $details ; 
	
		
	}

	
	function deleteMovieList($arr) {
		$id = $arr['movie_list_id'] ;
		
		$res = $this->db->delete('movie_list',array('id' => $id)   );
	
		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message deleteMovieList sucesss " ; 
             $details['status'] = "true";
			 $details['movie_list_id'] = $id;	
		}
		return  $details ; 
	
	}
	

	function addMovieList($arr) {
		
		$details = array();
		$member_id = $arr['member_id'] ;
		$name = $arr['name'] ;
		
		$data = array( 'user_id' => $member_id, 'name' =>  $name);
		$res = $this->db->insert('movie_list', $data);
	
		$movie_list_id = $this->db->insert_id() ; 
				
		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message addMovieList sucesss " ; 
             $details['status'] = "true";
			 $details['movie_list_id'] = $movie_list_id;
		}

		return  $details ; 
	
	}
	
	
	function addMovie($arr) {
		
		$details = array();
		$movie_list_id = $arr['movie_list_id'] ;
		$title = $arr['title'] ;
		$format = $arr['format'] ;
		$length = $arr['length'] ;
		$release_year = $arr['release_year'] ;
		$rating = $arr['rating'] ;
		
		$data = array( 'movie_list_id' => $movie_list_id,
					   'title' =>  $title ,
					   'format' =>  $format ,
					   'length' =>  $length ,
					   'release_year' =>  $release_year ,
					   'rating' =>  $rating ,
					   );

		$res = $this->db->insert('movies', $data);
		$movie_id = $this->db->insert_id() ; 
				
		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message addMovie sucesss " ; 
             $details['status'] = "true";
			 $details['movie_list_id'] = $movie_id;
		}

		return  $details ; 
	
	}
	
	
	function deleteMovie($arr) {
		
		$details = array();
		$movie_id = $arr['movie_id'] ;	
		$res = $this->db->delete('movies',array('id' => $movie_id)   );
				
		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message deleteMovie sucesss " ; 
             $details['status'] = "true";
			 $details['movie_list_id'] = $movie_id;
		}
		return  $details ; 
	
	}
	
	/*
	{"function":"getMovieList","parameters":{"member_id":"1","accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
	*/
	
	
	function getMovieList($arr) {
		$details = array();		
		$sql = "select a.id , a.name , b.title,b.format,b.length,b.release_year
				from movie_list a
					join movies b on a.id = b.movie_list_id
				";
				
		$qry = $this->db->query($sql);
		$res = $qry->result_array();

		if (!$res){
		  $msg = $this->db->_error_message();
		  $num = $this->db->_error_number();
		  $details['message'] = $this->db->_error_message();
          $details['status'] = "false";
		}
		else
		{
			 $details['message'] = "Message getMovieList sucesss " ; 
             $details['status'] = "true";
			 $details['data'] = $res;
			
		}
		return  $details ;
		
	}

}
