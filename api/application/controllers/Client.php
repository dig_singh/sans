<?php
//error_reporting(E_ALL);
//ini_set("display_errors","on");
class Client extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('client_model');
	}

	public function index() {

		if (isset($HTTP_RAW_POST_DATA)) {
			$json = $HTTP_RAW_POST_DATA;
		} else {
			$json = implode("\r\n", file('php://input'));
		}

		$array = json_decode($json, TRUE);
		$token = $array['token'];
		$function_name = $array['function'];
		$this->$function_name($array['parameters']);
	}

	
	function login($arr) {
             
				$email = $arr['email'];
				$password = $arr['password'];
				
				$email = "arunm@newagesmb.com";
				$password = "qwerty";
				
				
                if($array['email'] != ""){
                        $result = $this->client_model->login($array);       
						echo json_encode($result);
						
                }else {
                        $details['message'] = "Invalid Login Details";
                        $details['status'] = "false";
                        echo json_encode($details);
                }

        }
		
	
	 
	function getMovieList($arr) {
		$result = $this->client_model->getMovieList($arr);
		echo json_encode($result);

	}

	function deleteMovieList($arr) {
	    $result =   $this->client_model->deleteMovieList($arr);
		echo json_encode($arr);

	}

	/*
	 *
	 {"function":"addMovieList","parameters":{"member_id":"1","name":"Best Movie - 20011","accesskey":""},"token":""}
    */
	
	function addMovieList($arr) {
	    $result =   $this->client_model->addMovieList($arr);
		echo json_encode($result);

	}
	
	 // testting
	 
	 /*
	 *
	 {"function":"addMovie","parameters":{"member_id":"1","movie_list_id":7,"title":"Di1g","format":"DVD","length":"30","release_year":"2001","rating":"5","accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
	 
    */

	function addMovie($arr) {
		$result = $this->client_model->addMovie($arr);
		echo json_encode($result);
	}

	
	 
	 /*
	 *
	 {"function":"deleteMovie","parameters":{"member_id":"1","movie_id":7,"accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
	 
    */
	
	function deleteMovie($arr) {
		$result = $this->client_model->deleteMovie($arr);
		echo json_encode($result);
	}

	function getMovies($arr) {
		$result = $this->client_model->getMovieList($arr);
		echo json_encode($result);

	}

} // end class Client - GoTrashy
