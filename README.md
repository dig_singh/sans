Welcome to Sans Api Repository

A Movie database collection api based on codeigniter / mysql 

curl example : 

curl -X POST \
  http://52.23.25.132/api/client \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 8ca3ee2e-4ac6-4054-944b-55eabd9f9c9d' \
  -d '{"function":"getMovieList","parameters":{"member_id":"1","accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
'

curl -X POST \
  http://52.23.25.132/sans/api/client \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 53e1106b-5853-4d69-90d5-5f4d8971d1c5' \
  -d '{"function":"addMovieList","parameters":{"member_id":"1","name":"Best Movie - 2001","accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
'

curl -X POST \
  http://52.23.25.132/sans/api/client \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 7179256b-5793-4c46-97c5-6b17d8bfaf95' \
  -d '{"function":"deleteMovieList","parameters":{"member_id":"1", "movie_list_id" : 1 , accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
'
curl -X POST \
  http://52.23.25.132/sans/api/client \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: f6bd2299-a353-4bd4-b03e-cbfa0a13d83c' \
  -d '{"function":"addMovie","parameters":{"member_id":"1","movie_list_id":7,"title":"sssssss","format":"DVD","length":"30","release_year":"2001","rating":"5","accesskey":"9fafe833703a20d454d2b5a5167928150d6573dec452452f125edd4dc94b5e94"},"token":""}
'


